#!/usr/bin/env python3
# Copyright (c) 2019 The Bitcoin Core developers
# Copyright (c) 2019-2020 The Bitcoin developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

from glob import glob
from os.path import basename

names_raw = glob("*.raw")
print("Found " + str(len(names_raw)) + " .raw file(s) in working directory")
names_raw.sort()

names = []

for name_raw in names_raw:

    name = name_raw[:-4]
    name_cpp = name + ".cpp"
    name = basename(name)

    with open(name_raw, "rb") as file_raw, open(name_cpp, "w") as file_cpp:

        print("Converting " + name_raw + " to " + name_cpp + " ...")
        contents = file_raw.read()

        file_cpp.write("// DO NOT EDIT THIS FILE - it is machine-generated, use convert-raw-files.py to regenerate\n")
        file_cpp.write("\n")
        file_cpp.write("#include <cstdint>\n")
        file_cpp.write("#include <vector>\n")
        file_cpp.write("\n")
        file_cpp.write("static const unsigned char raw[] = {" + ",".join(map(lambda x: "0x{:02x}".format(x), contents)) + "};\n")
        file_cpp.write("\n")
        file_cpp.write("namespace benchmark {\n")
        file_cpp.write("namespace data {\n")
        file_cpp.write("\n")
        file_cpp.write("extern const std::vector<uint8_t> " + name + "(raw, raw + " + str(len(contents)) + ");\n")
        file_cpp.write("\n")
        file_cpp.write("} // namespace data\n")
        file_cpp.write("} // namespace benchmark\n")

    names.append(name)

if len(names):

    name_h = "../data.h"

    with open(name_h, "w") as file_h:

        print("Writing " + str(len(names)) + " declaration(s) to " + name_h + " ...")

        file_h.write("// DO NOT EDIT THIS FILE - it is machine-generated, use data/convert-raw-files.py to regenerate\n")
        file_h.write("\n")
        file_h.write("#ifndef BITCOIN_BENCH_DATA_H\n")
        file_h.write("#define BITCOIN_BENCH_DATA_H\n")
        file_h.write("\n")
        file_h.write("#include <cstdint>\n")
        file_h.write("#include <vector>\n")
        file_h.write("\n")
        file_h.write("namespace benchmark {\n")
        file_h.write("namespace data {\n")
        file_h.write("\n")
        for name in names:
            file_h.write("extern const std::vector<uint8_t> " + name + ";\n")
        file_h.write("\n")
        file_h.write("} // namespace data\n")
        file_h.write("} // namespace benchmark\n")
        file_h.write("\n")
        file_h.write("#endif // BITCOIN_BENCH_DATA_H\n")

    print("Done")

